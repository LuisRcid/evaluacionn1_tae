<%-- 
    Document   : index
    Created on : 24-09-2021, 08:11:18
    Author     : surro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculadora</title>
    </head>
    <body>
        <section style="text-align: center; font-size: x-large" >
            <h1>Ingrese los numeros para calcular el interes simple</h1>
            <form action="CalculoController" method="POST">
                <label>Ingrese capital: <input type="number" name="capital" id="idcapital" required autofocus min="0"></label>
                <br>
                <label>Ingrese la tasa de interes anual: <input type="number" name="interesanual" id="idinteresanual" required min="0"></label>
                <br>
                <label>Ingrese la cantidad de años: <input type="number" name="anios" id="idanios" required min="0"></label>
                <br>
                <br>
                <button type="submit" name="btncalcular" style="font-size: x-large; width: 50%">Calcular</button>
            </form>
        </section>
    </body>
</html>
