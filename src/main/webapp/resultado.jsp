<%-- 
    Document   : resultado
    Created on : 24-09-2021, 09:33:38
    Author     : surro
--%>

<%@page import="com.mycompany.evaluacionn1_tae.modelo.Calculo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
  Calculo calcu = (Calculo) request.getAttribute("calculo");
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultado</title>
    </head>
    <body>
        <section style="text-align: center; font-size: x-large">
            <h1>Interes simple</h1>
            <label>Un capital de $<%= calcu.getCapital()%>
                a una tasa anual de <%= Math.round(calcu.getInteresanual())%>%
                en <%=calcu.getAnio()%> años
                genera un interes simple de $<%= calcu.interesSimpleProducido()%></label>
        </section>
    </body>
</html>
