
package com.mycompany.evaluacionn1_tae.controller;

import com.mycompany.evaluacionn1_tae.modelo.Calculo;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author surro
 */
@WebServlet(name = "CalculoController", urlPatterns = {"/CalculoController"})
public class CalculoController extends HttpServlet {


  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    try ( PrintWriter out = response.getWriter()) {
      /* TODO output your page here. You may use following sample code. */
      out.println("<!DOCTYPE html>");
      out.println("<html>");
      out.println("<head>");
      out.println("<title>Servlet CalculoController</title>");
      out.println("</head>");
      out.println("<body>");
      out.println("<h1>Servlet CalculoController at " + request.getContextPath() + "</h1>");
      out.println("</body>");
      out.println("</html>");
    }
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
          String capital = request.getParameter("capital");
          String interesanual = request.getParameter("interesanual");
          String anios = request.getParameter("anios");
          
          Calculo cal = new Calculo(Integer.parseInt(capital), Integer.parseInt(anios), Double.parseDouble(interesanual));
          cal.interesSimpleProducido();
          
          request.setAttribute("calculo", cal);
          request.getRequestDispatcher("resultado.jsp").forward(request, response);
  }

  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>

}
