package com.mycompany.evaluacionn1_tae.modelo;


public class Calculo {
  private int capital, anio;
  private double interesanual;

  public Calculo() {
  }

  public Calculo(int capital, int anio, double interesanual) {
    this.capital = capital;
    this.anio = anio;
    this.interesanual = interesanual;
  }
  

  public int getCapital() {
    return capital;
  }

  public void setCapital(int capital) {
    this.capital = capital;
  }

  public int getAnio() {
    return anio;
  }

  public void setAnio(int anio) {
    this.anio = anio;
  }

  public double getInteresanual() {
    return interesanual;
  }

  public void setInteresanual(double interesanual) {
    this.interesanual = interesanual;
  }
  
  public int interesSimpleProducido(){
    return (int)(capital*(interesanual/100)*anio);
  }
  
}
